<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RobotController extends Controller
{
    private $tableMatrix = array();
    private $robot = [
        'in' => 'images/blank.png',
        'out' => 'images/robotmain.png'
    ];
    private $commandOutput = "";
    private $robotStatus = "out";
    private $robotDirection = [
        'NORTH' => 'images/north.png',
        'SOUTH' => 'images/south.png',
        'EAST' => 'images/east.png',
        'WEST' => 'images/west.png',
    ];
    private $robotPosition = [
        'col' => 'NA',
        'row' => 'NA',
        'direction' => 'NA'
    ];

    public function __construct()
    {
        $this->initBoard();
    }

    public function indexPage()
    {
        $data['matrix'] = $this->tableMatrix;
        $data['outside'] = $this->robot['out'];
        $data['commandoutput'] = $this->commandOutput;
        $data['robotstatus'] = $this->robotStatus;
        $data['robotposition'] = $this->robotPosition;
        return view('tablematrix', $data);
    }
    
    public function processCommand(Request $request)
    {
        $robotCommand=explode(' ', strtoupper($request->input('robotcommand')));
        $this->commandOutput = $request->input('commandoutput');
        $this->robotStatus = $request->input('robotstatus');
        $this->commandOutput(chr(13) . "command : " . $request->input('robotcommand') . " : ");
        $this->robotPosition['direction'] = $request->input('direction');
        $this->robotPosition['row'] = $request->input('row');
        $this->robotPosition['col'] = $request->input('col');

        switch($robotCommand[0])
        {
            case 'PLACE' : 
                $placeParameters = explode(",", $robotCommand[1]);
                $this->placeRobot($placeParameters[0], $placeParameters[1], $placeParameters[2]);
            break;

            case 'MOVE' :
                if($this->robotStatus=='in')
                    $this->moveRobot();
            break;

            case 'LEFT' :
            case 'RIGHT' :
                if($this->robotStatus=='in')
                    $this->rotateRobot($robotCommand[0]);
            break;

            case 'REPORT' :
                if($this->robotStatus=='in')
                {
                    $this->drawRobot($request->input('row'), $request->input('col'), $request->input('direction'));
                    $data['report']='true';
                }
            break;

            default : 
            break;
        }

        $data['matrix'] = $this->tableMatrix;
        $data['outside'] = $this->robot[$this->robotStatus];
        $data['commandoutput'] = $this->commandOutput;
        $data['robotstatus'] = $this->robotStatus;
        $data['robotposition'] = $this->robotPosition;
        return view('tablematrix', $data);
    }

    public function placeRobot($row, $col, $direction)
    {
        if($col < 0 || $col >= 5 || $row < 0 || $row >= 5)
        {
            $this->commandOutput("error");
            $this->robotStatus = "out";
        }
        else
        {
            $this->commandOutput("ok");
            $this->robotStatus = "in";
            $this->drawRobot($row, $col, $direction);
        }
        return;
    }

    public function moveRobot()
    {
        $row = $this->robotPosition['row'];
        $col = $this->robotPosition['col'];
        $direction = $this->robotPosition['direction'];
        switch($direction)
        {
            case 'NORTH' :
                if($row < 4)
                    $row++;
            break;

            case 'SOUTH' :
                if($row > 0)
                    $row--;
            break;

            case 'EAST' :
                if($col < 4)
                    $col++;
            break;

            case 'WEST' :
                if($col > 0)
                    $col--;
            break;

            default :
            break;
        }
        $this->drawRobot($row, $col, $direction);
        return;
    }

    public function rotateRobot($side)
    {
        $row = $this->robotPosition['row'];
        $col = $this->robotPosition['col'];
        $direction = $this->robotPosition['direction'];

        if($side=='LEFT')
        {
            switch($direction)
            {
                case 'NORTH' : $newDirection = "WEST"; break;
                case 'WEST' : $newDirection = "SOUTH"; break;
                case 'SOUTH' : $newDirection = "EAST"; break;
                case 'EAST' : $newDirection = "NORTH"; break;
            }
        }
        else
        {
            switch($direction)
            {
                case 'NORTH' : $newDirection = "EAST"; break;
                case 'WEST' : $newDirection = "NORTH"; break;
                case 'SOUTH' : $newDirection = "WEST"; break;
                case 'EAST' : $newDirection = "SOUTH"; break;
            }
        }
        $this->drawRobot($row, $col, $newDirection);
        return;
    }

    public function drawRobot($row, $col, $direction)
    {
        $this->tableMatrix[4 - $row][$col] = $this->robotDirection[$direction];
        $this->robotPosition['direction'] = $direction;
        $this->robotPosition['row'] = $row;
        $this->robotPosition['col'] = $col;
    }

    public function initBoard()
    {
        for($row=0; $row<=4; $row++)
        {
            for($col=0; $col<=4; $col++)
            {
                $this->tableMatrix[$row][$col]=$this->robot['in'];
            }
        }
        return;
    }

    public function commandOutput($message)
    {
        $this->commandOutput .= $message;
        return;
    }
}
