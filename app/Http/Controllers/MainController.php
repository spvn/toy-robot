<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    private $robotPosition = [
        'posx' => 0,
        'posy' => 0,
        'facing' => 'NA',
        'placed' => false
    ];

    private $commandError = false;

    public function indexPage()
    {
        return view('robot',[
            'message' => "NO"
        ]);
    }

    public function processCommands(Request $request)
    {
        $commandList = explode("\r\n",strtoupper($request->input('robotcommand')));
        $message="NO";
        foreach($commandList as $command)
        {
            $commandParts = explode(" ", $command);

            switch($commandParts[0])
            {
                case 'PLACE' :
                    $robotPlace = explode(",", $commandParts[1]);
                    $this->placeRobot($robotPlace[0], $robotPlace[1], $robotPlace[2]);
                break;

                case 'MOVE' :
                    $this->moveRobot();
                break;

                case 'LEFT' :
                case 'RIGHT' :
                    $this->turnRobot($commandParts[0]);
                break;

                case 'REPORT' :
                    $message="YES";
                break;
            }
            if($this->commandError)
                break;
        }
        $data['position']=$this->robotPosition;
        $data['message'] = $message;
        return view("robot", $data);
    }

    public function placeRobot($posX, $posY, $facing)
    {
        if(!$this->commandError)
        {
            if($posX >= 0 && $posX < 5 && $posY >= 0 && $posY < 5)
            {
                $this->robotPosition['posx'] = $posX;
                $this->robotPosition['posy'] = $posY;
                $this->robotPosition['facing'] = $facing;
                $this->robotPosition['placed'] = "true";
            }
            else
            {
                $this->commandError = true;
                $this->robotPosition['placed'] = "false";
            }
        }
        return;
    }

    public function moveRobot()
    {
        if(!$this->commandError && $this->robotPosition['placed']=="true")
        {
            switch($this->robotPosition['facing'])
            {
                case "NORTH" :
                    if($this->robotPosition['posy'] < 4)
                        $this->robotPosition['posy'] = $this->robotPosition['posy'] + 1;
                break;

                case "SOUTH" :
                    if($this->robotPosition['posy'] > 0)
                        $this->robotPosition['posy'] = $this->robotPosition['posy'] - 1;
                break;

                case "EAST" :
                    if($this->robotPosition['posx'] < 4)
                        $this->robotPosition['posx'] = $this->robotPosition['posx'] + 1;
                break;

                case "WEST" :
                    if($this->robotPosition['posx'] > 0)
                        $this->robotPosition['posx'] = $this->robotPosition['posx'] - 1;
                break;
            }
        }
        return;
    }

    public function turnRobot($side)
    {
        if(!$this->commandError && $this->robotPosition['placed']=="true")
        {
            $direction = $this->robotPosition['facing'];
            if($side=='LEFT')
            {
                switch($direction)
                {
                    case 'NORTH' : $newDirection = "WEST"; break;
                    case 'WEST' : $newDirection = "SOUTH"; break;
                    case 'SOUTH' : $newDirection = "EAST"; break;
                    case 'EAST' : $newDirection = "NORTH"; break;
                }
            }
            else
            {
                switch($direction)
                {
                    case 'NORTH' : $newDirection = "EAST"; break;
                    case 'WEST' : $newDirection = "NORTH"; break;
                    case 'SOUTH' : $newDirection = "WEST"; break;
                    case 'EAST' : $newDirection = "SOUTH"; break;
                }
            }
            $this->robotPosition['facing']= $newDirection;
        }
        return;
    }
}
