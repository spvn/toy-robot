<!DOCTYPE html>
<html>
    <head>
        <title>Toy Robot</title>
    </head>
    <body>
        <form method="POST">
            @csrf
            <p>Enter commands and click on Submit</p>
            <textarea name="robotcommand" rows="20"></textarea><br/>
            <input type="submit"/>
        </form>
        @if($message=="YES")
        <p>{{ $position['posx'] }}, {{ $position['posy'] }}, {{ $position['facing'] }}</p>
        @endif
    </body>
</html>