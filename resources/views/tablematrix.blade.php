<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <title>Toy Robot</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h4>Toy Robot</h4>
                </div>
            </div>
            <div class="row">
            <div class="col-6">
                <div class="table-responsive">
                    <table>
                    @for ($row=0; $row<=4; $row++)
                        <tr>
                        @for ($col=0; $col<=4; $col++)
                            <td><image src="{{ asset($matrix[$row][$col]) }}" class="img-fluid"/></td>
                        @endfor
                        </tr>
                    @endfor
                    </table>
                </div>
            </div>
            <div class="col-6">
                <div class="row">
                    <div class="col-4">
                        <img src="{{ asset($outside) }}" class="img-fluid"/>
                    </div>
                    <div class="col-8">
                        {{-- $robotstatus --}}
                    </div>
                </div>
                <div class="row"><br/></div>
                <div class="row">
                    <form method="POST" action="{{ route('process') }}">
                        @csrf
                        <input type="text" class="form-control form-control-lg" name="robotcommand" size="50" autofocus/>
                        <input type="hidden" name="robotstatus" value="{{ $robotstatus }}"/>
                        <input type="hidden" name="direction" value="{{ $robotposition['direction'] }}"/>
                        <input type="hidden" name="col" value="{{ $robotposition['col'] }}"/>
                        <input type="hidden" name="row" value="{{ $robotposition['row'] }}"/>
                        <br/>
                        <button type="submit" class="btn btn-primary mb-2">Submit</button>
                        <br/>
                        <textarea class="form-control form-control-sm" name="commandoutput" rows="10">{{ $commandoutput }}</textarea>                        
                    </form>
                </div>
                @if (isset($report))
                <div class="row">
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>Robot Position </strong> {{ $robotposition['row'] }}, {{ $robotposition['col'] }}, {{ $robotposition['direction'] }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                @endif
            </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body>
</html>