## Toy Robot

Toy Robot is a simple Laravel based web application to move a robot on a 5 x 5 matrix table

Before installing Toy Robot please ensure you have PHP 7.1.3 or above

## Running the solution

Two solutions are included

* Text based solution - **http://host/**
* GUI solution - **http://host/gfx**