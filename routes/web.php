<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/','MainController@indexPage')->name('indexpage');
Route::post('/','MainController@processCommands')->name('processcommands');

Route::get('/gfx','RobotController@indexPage')->name('home');
Route::post('/gfx','RobotController@processCommand')->name('process');
